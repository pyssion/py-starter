# Py-Starter

This is a simple starter project for Python project.

## Tech Stack

- mainly `pipenv`

## Managing Dependencies

We use `pipenv` to manage dependencies, so

- make sure you have the correct version of Python on your system (see [Pipfile](./Pipfile))
- run `pipenv install` to install dependencies
- make sure pipenv is activated with `pipenv shell`

## Running the Project

The main package is `paw_pkg`, so you can run it with:

```shell
python -m paw_pkg
```

or, we have a module named `run.py`, you can also start with it:

```shell
python run.py
```

## A Word on Module and Package

A module or a package in Python, is a way of keeping namespace clean. The subtle
difference is, a module is a `.py` file whereas a package is a directory
containing a `__init__.py` file.

Apparently, you should use a module (single file) for simple tasks, but keep
heavy logic in a package, or a package with submodules and sub-packages.

When it comes to resolving modules, Python usually searches the directory where
its script is provided (and other system places, see [official doc](https://docs.python.org/3/tutorial/modules.html)
for more).

That's why,

1. when we want a single starting `.py` script, it has to be in the root directory,
    so that Python can locate our other packages (our `paw_pkg` in this case).
2. when we try to run a package, a `-m` flag is required

For a module / package to treat itself as the *main* entry, use this:

```python
if __name__ is "__main__":
    print("Yes we are the main module")
```

and if you put this in a package, the *main* should be a file named `__main__.py`
by convention.
